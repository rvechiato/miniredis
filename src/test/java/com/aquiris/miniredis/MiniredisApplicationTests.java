package com.aquiris.miniredis;

import com.aquiris.miniredis.scopedb.RedisDB;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MiniredisApplicationTests {

    @Autowired
    private RedisDB redisdb;

    @Test
    public void get_test() {
        //SET
        redisdb.set("mykey", "hello");

        //GET
        assertThat(redisdb.get("mykey")).isEqualTo("hello");
    }

    @Test
    public void set_with_ex_test() {
        //SET EX
        redisdb.set("mykeytemp", "hello temp", 2);

        try {
            Thread.sleep(1000);
            assertThat(redisdb.get("mykeytemp")).isEqualTo("hello temp");
            Thread.sleep(2000);
            assertThat(redisdb.get("mykeytemp")).isEqualTo("(nil)");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void del_test() {
        //DEL
        redisdb.set("data", "123456");
        assertThat(redisdb.del("data")).isEqualTo(1);

        redisdb.set("data1", "123456");
        redisdb.set("data2", "abc");
        assertThat(redisdb.del("data1", "data2")).isEqualTo(2);


        //chave não existente
        redisdb.set("data", "123456");
        redisdb.set("data1", "abc");
        assertThat(redisdb.del("data1", "data2")).isEqualTo(1);
    }

    @Test
    public void dbsize_test() {
        //Massa para teste de atualização e de dbsize
        redisdb.set("mykey1", "1");
        redisdb.set("mykey2", "abc");
        redisdb.set("mykey3", "3");
        redisdb.set("mykey", "4");
        redisdb.set("mykey", "5");


        //dbsize
        assertThat(redisdb.dbsize()).isEqualTo(5);
    }

    @Test
    public void incr_test() {
        redisdb.set("mykey1", "1");
        redisdb.set("mykey2", "abc");
        redisdb.set("mykey3", "3");
        redisdb.set("mykey", "4");
        redisdb.set("mykey", "5");

        //incr
        assertThat(redisdb.incr("mykey")).isEqualTo("6"); //incremento em chave já existente
        assertThat(redisdb.incr("mykeynew")).isEqualTo("0");//incremento em chave não existente
        assertThat(redisdb.incr("mykey2")).isEqualTo("ERROR - Format invalid!"); //formate de dado errado/incorreto
    }

    @Test
    public void zadd_test() {
        //ZADD
        assertThat(redisdb.zadd("myzset", 1, "one")).isEqualTo(1);
    }

    @Test
    public void ZCARD_test() {

        redisdb.zadd("myzset", 1, "one");//0
        redisdb.zadd("myzset", 2, "two");//1
        redisdb.zadd("myzset", 1, "uno");//2
        redisdb.zadd("myzset", 4, "four");//3
        redisdb.zadd("myzset", 13, "thirteen");//4

        //ZCARD
        assertThat(redisdb.zcard("myzset")).isEqualTo(5);
    }

    @Test
    public void zrank_test() {
        redisdb.zadd("myzset", 1, "one");//0
        redisdb.zadd("myzset", 2, "two");//1
        redisdb.zadd("myzset", 1, "uno");//2
        redisdb.zadd("myzset", 4, "four");//3
        redisdb.zadd("myzset", 13, "thirteen");//4

        assertThat(redisdb.zrank("myzset", "uno")).isEqualTo("1");
    }

    @Test
    public void zrange_test() {
        redisdb.zadd("myzset", 1, "one");//0
        redisdb.zadd("myzset", 2, "two");//1
        redisdb.zadd("myzset", 1, "uno");//2
        redisdb.zadd("myzset", 4, "four");//3
        redisdb.zadd("myzset", 13, "thirteen");//4

        assertThat(redisdb.zrange("myzset", 0, -1)).containsOnly("one", "uno", "two", "four", "thirteen");
        assertThat(redisdb.zrange("myzset", 2, -1)).containsOnly("two", "four", "thirteen");
        assertThat(redisdb.zrange("myzset", 2, 3)).containsOnly("two");
        assertThat(redisdb.zrange("myzset", 2, 1)).containsOnly();
        assertThat(redisdb.zrange("myzset", -3, -1)).containsOnly("two", "four", "thirteen");
    }

}
