package com.aquiris.miniredis.commons;

import java.util.concurrent.Callable;
public class ControlData implements Callable<String> {
    private String key;
    private  Long time;
    public ControlData(String key, Long time) {
        this.key = key;
        this.time = time;
    }

    @Override
    public String call() throws Exception {
        Thread.sleep(this.time);
        return this.key;
    }
}
