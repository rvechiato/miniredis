package com.aquiris.miniredis.commons;

import java.util.List;

public interface IRedis<T> {
    String set(String key, String value);
    String set(String key, String value, Integer ex);
    String get(String key);
    Integer del(String... keys);
    Integer dbsize();
    String incr(String key);
    Integer zadd(String key,Integer score, String value);
    Integer zcard(String key);
    String zrank(String key,String value);
    List<T> zrange(String key,Integer start, Integer stop);
}
