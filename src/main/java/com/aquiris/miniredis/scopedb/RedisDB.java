package com.aquiris.miniredis.scopedb;

import com.aquiris.miniredis.commons.IRedis;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/*
    Classe que fará a emulação do db redis
    aqui será armazenado os dados e mantidos na memória
 */
@Component
public class RedisDB implements IRedis<String> {

    //Variavel onde será armazenado os valores
    //defini dois map um para cada tipos de dado
    Map<String, String> db = new HashMap<>();
    Map<String, LinkedHashMap<String, Integer>> dbz = new HashMap<>();

    @Override
    public String set(String key, String value) {
        //verificar se ja existe a chave, se já existir substituir pelo novo valor
        if (StringUtils.isBlank(value)) {
            if (!StringUtils.isBlank(this.get(key))) {
                return this.get(key);
            }
        }
        synchronized(this){
            this.db.put(key, value);
        }

        return "OK";
    }

    /**
     * Adicionar a chave no bd, e marcar ela com um tempo para ser excluida
     * Add uma thread para controlar o tempo de cada dado no bd
     *
     * @param key
     * @param value
     * @param ex
     * @return
     */
    @Override
    public String set(String key, String value, Integer ex) {

        try {
            this.set(key, value);
            setTimeForExpireData(key, ex);
        } catch (Exception e) {

        }
        return "OK";
    }

    /**
     * ]
     * Abter o valor atravez da chave
     *
     * @param key
     * @return
     */
    @Override
    public String get(String key) {


        String value = this.db.get(key);
        return !StringUtils.isBlank(value) ? value : "(nil)";
    }

    /**
     * Possibilitar a remoção de multiplas chaves
     * Retornar a quantidade de chaves removida -return integer
     *
     * @param keys
     * @return
     */
    @Override
    public Integer del(String... keys) {
        Integer keyRemoveQty = 0;
        for (String key : keys) {
            String data = get(key);
            if(!StringUtils.isBlank(data) && !data.equals("(nil)")){
                this.db.remove(key);
                keyRemoveQty++;
            }
        }
        return keyRemoveQty;
    }

    /**
     * Return the number of keys in the currently-selected database.
     *
     * @return
     */
    @Override
    public Integer dbsize() {
        return this.db.size();
    }

    /**
     * Increments the number stored at key by one.
     *
     * @param key
     * @return
     */
    @Override
    public String incr(String key) {
        //Increments the number stored at key by one.
        //If the key does not exist, it is set to 0 before performing the operation.
        //An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.
        //This operation is limited to 64 bit signed integers.
        try {

            String value = this.db.get(key);
            if (!StringUtils.isBlank(value)) {
                Integer number = Integer.parseInt(value);
                number++;
                this.set(key, number.toString());
            } else {
                this.set(key, "0");
            }
            return this.db.get(key);
        } catch (NumberFormatException e) {
            return "ERROR - Format invalid!";
        }
    }

    /**
     * Adds all the specified members with the specified scores to the sorted set stored at key
     *
     * @param key
     * @param score
     * @param value
     * @return
     */

    @Override
    public Integer zadd(String key, Integer score, String value) {
        //Ordenar os itens incrementando
        //Vertificar se a chave existe
        //se não existir a chave incluir uma nova no bd
        //retorna 1 pq sempre faz um por vez

        LinkedHashMap<String, Integer> data = this.dbz.get(key);
        if (data != null) {
            data.put(value, score);
            this.dbz.put(key, sortZMap(data));
        } else {
            LinkedHashMap<String, Integer> nData = new LinkedHashMap<>();
            nData.put(value, score);
            this.dbz.put(key, nData);
        }

        return 1;
    }

    @Override
    public Integer zcard(String key) {
        Map<String, Integer> data = this.dbz.get(key);
        if (data != null) {
            return data.size();
        }
        return 0;
    }

    /**
     * Returns the sorted set cardinality (number of elements) of the sorted set stored at key.
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public String zrank(String key, String value) {
        //Obter os dados
        //verficar se existe alguma chave com o valor, se não exister ja retorna nil
        //verificar qual a posição do elemanto no rank, ao encontrar para e ja retorna

        Map<String, Integer> dados = this.dbz.get(key);
        boolean find = dados.keySet().stream().filter(k -> k.equals(value)).findAny().isPresent();

        if (!find)
            return "nil";

        if (dados != null) {
            AtomicInteger pos = new AtomicInteger(-1);

            for (Map.Entry<String, Integer> node : dados.entrySet()) {
                pos.getAndIncrement();
                if (node.getKey().equals(value)) {
                    find = true;
                    break;
                }
            }
            if (find) {
                Integer positon = pos.get();
                return positon.toString();
            }
        }

        return "(nil)";
    }


    /**
     * Returns the specified range of elements in the sorted set stored at key. The elements are considered to be ordered from the lowest to the highest score.
     * Lexicographical order is used for elements with equal score.
     * @param key
     * @param start
     * @param stop
     * @return
     */
    @Override
    public List<String> zrange(String key, Integer start, Integer stop) {
        //talvez seja necesssario invocar uma ordencao aqui para garantir

        LinkedHashMap<String, Integer> data = this.dbz.get(key);
        List<String> result = new ArrayList<>();

        List<String> sorted = new ArrayList<String>(data.keySet());

        if (data != null) {
            //validações de OffSet
            if (stop > data.size())
                stop = data.size();
            else if (stop < 0) {
                if (stop == -1)
                    stop = data.size();
                if (stop < -1)
                    stop = data.size() + stop;
            }

            if (start < 0)
                start = data.size() + start;

            if (start > stop)
                return result;

            try {
                for (int i = start; i < stop; i++) {
                    result.add(sorted.get(i));
                }
            } catch (IndexOutOfBoundsException e) {
                return new ArrayList<>();
            }
        }
        return result;
    }


    /**
     * Metodo responsável por criar as threads que irão controlar os registros temporarios no db
     *
     * @param key
     * @param ex
     */
    private void setTimeForExpireData(String key, Integer ex) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //ex este em segundos, por isso deve ser feita a conversão para millisegundos
                    Thread.sleep(ex * 1000);
                    del(key);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * Realizar a ordenação dos elemantos de uma lista
     * @param data
     * @return
     */
    private LinkedHashMap<String, Integer> sortZMap(LinkedHashMap<String, Integer> data) {
        //Obter os dados e converter pora uma list
        //relizar a ordenação
        //reposicionar os dados na lista

        List<Map.Entry<String, Integer>> entries = new ArrayList<>(data.entrySet());

        Collections.sort(entries, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return a.getValue().compareTo(b.getValue());
            }
        });

        LinkedHashMap<String, Integer> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : entries) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }
}
