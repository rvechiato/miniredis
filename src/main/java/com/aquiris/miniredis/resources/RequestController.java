package com.aquiris.miniredis.resources;

import com.aquiris.miniredis.scopedb.RedisDB;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value="/redis")
public class RequestController {

    @Autowired
    private RedisDB redisdb;

    @RequestMapping(value ="/set", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<String> set(@RequestParam(name = "key") String key,
                      @RequestParam(name = "value") String value){
        if(StringUtils.isBlank(key))
            return  ResponseEntity.notFound().build();
            return set(key,value, null);

    }

    @RequestMapping(value ="/set", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<String> set(@RequestParam(name = "key") String key,
                      @RequestParam(name = "value") String value,
                      @RequestParam(name = "ex") Integer ex){
        if(StringUtils.isBlank(key))
            return  ResponseEntity.notFound().build();

        if(ex != null && ex > 0)
            return ResponseEntity.ok().body(redisdb.set(key,value,ex));
        return ResponseEntity.ok().body(redisdb.set(key,value));
    }


    @RequestMapping(value ="/get", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<String> get(@RequestParam(name = "key") String key){
        if(StringUtils.isBlank(key))
            return  ResponseEntity.notFound().build();

        return ResponseEntity.ok().body(redisdb.get(key));
    }

    @RequestMapping(value ="/del", method = RequestMethod.DELETE)
    public  @ResponseBody ResponseEntity<Integer> del(@RequestParam(name = "key") String... key){
        if(key.length == 0)
            return  ResponseEntity.ok().body(0);
        return  ResponseEntity.ok().body(redisdb.del(key));
    }

    @RequestMapping(value ="/dbsize", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Integer> dbsize(){
        return  ResponseEntity.ok().body(redisdb.dbsize());
    }

    @RequestMapping(value ="/incr", method = RequestMethod.PATCH)
    public @ResponseBody ResponseEntity<String> incr(@RequestParam(name = "key") String key){
        if(StringUtils.isBlank(key))
            return  ResponseEntity.notFound().build();
        return ResponseEntity.ok().body(redisdb.incr(key).toString());
    }


    @RequestMapping(value ="/zadd", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Integer> zadd(@RequestParam(name = "key") String key,
                                                      @RequestParam(name = "score") Integer score,
                                                      @RequestParam(name = "value") String value){
        if(StringUtils.isBlank(key))
            return ResponseEntity.ok().body(0);;

        if(score != null && !StringUtils.isBlank(value))
            return  ResponseEntity.ok().body(redisdb.zadd(key,score,value));

        return  ResponseEntity.ok().body(0);
    }

    @RequestMapping(value ="/zcard", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<?> zcard(@RequestParam(name = "key") String key){
        return  ResponseEntity.ok().body(redisdb.zcard(key));
    }

    @RequestMapping(value ="/zrank", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<?> zrank(@RequestParam(name = "key") String key,
                                                 @RequestParam(name = "value") String value){
        return  ResponseEntity.ok().body(redisdb.zrank(key,value));
    }

    @RequestMapping(value ="/zrange", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<?>  zrange(@RequestParam(name = "key") String key,
                                                   @RequestParam(name = "start") Integer start,
                                                   @RequestParam(name = "stop") Integer stop){

        return  ResponseEntity.ok().body(redisdb.zrange(key,start,stop));
    }
}
